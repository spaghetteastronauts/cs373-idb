import {
  BrowserRouter,
  Route,
  Link,
  Switch
} from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import { TextField, InputAdornment, Hidden, Button } from '@material-ui/core/';
import { useState, useEffect } from "react";
import SearchIcon from '@material-ui/icons/Search';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import axiosInstance from "./classes/AxiosInstance";

import AboutPage from './components/About/AboutPage';
import SpeciesCards from './components/Species/SpeciesCards';
import EnvironmentsCards from './components/Environments/EnvironmentsCards';
import ThreatsCards from './components/Threats/ThreatsCards';
import Splash from "./components/Splash/Splash";
import SearchResults from "./components/GlobalSearch/SearchResults";

import HomeIcon from '@material-ui/icons/Home';
import PetsIcon from '@material-ui/icons/Pets';
import NatureIcon from '@material-ui/icons/Nature';
import WarningIcon from '@material-ui/icons/Warning';
import InfoIcon from '@material-ui/icons/Info';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import Threat from "./components/Threats/Threat";
import Species from "./components/Species/Species";
import Environment from "./components/Environments/Environment";
import logo from './static/images/logo.svg';
import PokemonTeam from "./components/Pokemon/PokemonTeam";

const useStyles = makeStyles((theme) => ({
  content: {
    background: '#d5f4e6',
    minHeight: '100vh',
    padding: '0px',
    fixed: '1'
  },
  toolbar: {
    background: '#80ced6',
    // width: "100%"
  },

  input: {
    display: 'inline',
    alignSelf: 'center',
    marginLeft: 'auto',
    marginRight: '0px',
    backgroundColor: 'white'

  },
  tab: {
    minWidth: "45px"
  },
  button: {
    marginLeft: '5px',
    alignSelf: 'center',
    marginRight: '50px',
    backgroundColor: 'white'
  }
}));

const Hide = ({text}) => {
  return (
    <Hidden xsDown>{text}</Hidden>
  )
}



function App() {
  const classes = useStyles();

  const [search, setSearch] = useState('');

  const [species, setSpecies] = useState(null);
  const [environments, setEnvironments] = useState(null);
  const [threats, setThreats] = useState(null);

  useEffect(() => {
      async function fetchSpecies() {
          let response = await axiosInstance.get("/api/species");
          setSpecies(response.data);
      }
      async function fetchEnvironments() {
          let response = await axiosInstance.get("/api/environments");
          setEnvironments(response.data);
      }
      async function fetchThreats() {
          let response = await axiosInstance.get("/api/threats");
          setThreats(response.data);
      }
      fetchSpecies();
      fetchEnvironments();
      fetchThreats();
  }, []);


  const handleSearch = event => {
    setSearch(event.target.value);
  }

  return (
    <div className="App">
      <Container className={classes.content}>
        <BrowserRouter>
          <Route
            path="/"
            render={({ location }) => (
              <div className="App">
                <Box display="flex" flexDirection="row" paddingLeft="20px">
                  <img className="logo" src={logo} alt="My_Logo" width="48"></img>
                  <Hidden xsDown><h1>InDangerEd</h1></Hidden>
                  <div className={classes.input}>
                    <TextField
                      label='Search Site...'
                      variant='outlined'
                      onChange={handleSearch}
                      size='small'
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="start">
                            <SearchIcon />
                          </InputAdornment>
                        )
                      }}
                    />
                  </div>
                  <Button className={classes.button} variant="outlined" disableElevation component={Link} to={`/search/${search}`}>Go</Button>
                </Box>
                <AppBar position="static" className={classes.toolbar}>
                  <Tabs value={shortenURL(location.pathname)} variant="fullWidth">
                    {/* <Tabs value={shortenURL(location.pathname)}> */}

                    <Tab
                      value={"/"}
                      label={<Hide text="Home"/>}
                      component={Link}
                      to="/"
                      icon={<HomeIcon />}
                      className={classes.tab}
                    />
                    <Tab
                      value={"/species"}
                      label={<Hide text="Species"/>}
                      component={Link}
                      to="/species"
                      icon={<PetsIcon />}
                      className={classes.tab}
                    />
                    <Tab
                      value={"/environments"}
                      label={<Hide text="Environments"/>}
                      component={Link}
                      to="/environments"
                      icon={<NatureIcon />}
                      className={classes.tab}
                    />
                    <Tab
                      value={"/threats"}
                      label={<Hide text="Threats"/>}
                      component={Link}
                      to="/threats"
                      icon={<WarningIcon />}
                      className={classes.tab}
                    />
                    <Tab
                      value={"/about"}
                      label={<Hide text="About" />}
                      component={Link}
                      to="/about"
                      icon={<InfoIcon />}
                      className={classes.tab}
                    />
                    <Tab
                      value={"/pokemon"}
                      label={<Hide text="Pokémon"/>}
                      component={Link} to="/pokemon"
                      icon={<RemoveCircleOutlineIcon />}
                      className={classes.tab}
                    />
                  </Tabs>
                </AppBar>

                <Switch>
                  {/* <Redirect exact from="/" to="/home" /> */}
                  <Route path="/environments/:id">
                    <Environment />
                  </Route>
                  <Route path="/species/:id">
                    <Species />
                  </Route>
                  <Route path="/threats/:id">
                    <Threat />
                  </Route>
                  <Route path="/species">
                    <SpeciesCards species={species} environments={environments} threats={threats}/>
                  </Route>
                  <Route path="/environments">
                    <EnvironmentsCards species={species} environments={environments} threats={threats}/>
                  </Route>
                  <Route path="/threats">
                    <ThreatsCards species={species} environments={environments} threats={threats}/>
                  </Route>
                  <Route path="/about">
                    <AboutPage />
                  </Route>
                  <Route path="/pokemon">
                    <PokemonTeam />
                  </Route>
                  <Route path="/search/:search">
                    <SearchResults species={species} environments={environments} threats={threats}/>
                  </Route>
                  <Route path="/">
                    <Splash />
                  </Route>
                </Switch>
              </div>
            )}
          />


        </BrowserRouter>
      </Container>
    </div>
  );
}

/**
 * Given a URL, returns only the first portion.
 * i.e. "/species/Blue Whale" becomes "/species",
 * "/species" becomes "/species"
 * 
 * @param {String} path 
 * @returns {String}
 */
function shortenURL(path) {
  // console.log('path :>> ', path);
  let urlSegments = path.split("/");
  return "/" + urlSegments[1];
}

export default App;


