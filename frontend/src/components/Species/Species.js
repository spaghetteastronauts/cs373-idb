import { Link, useParams } from "react-router-dom";
import  { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import { Paper, TableBody } from "@material-ui/core";
import TableRow from '@material-ui/core/TableRow';
import { TableHead } from "@material-ui/core";
import axiosInstance from "../../classes/AxiosInstance";
import { CircularProgress } from "@material-ui/core";
import GoogleMap from "./GoogleMap";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    margin: "1em",
    padding: "1em",
    width: "fit-content"
  },
  gridList: {
    width: '100vw'
  }
}));

const Species = () => {
  const classes = useStyles();

  let { id } = useParams();
  const [species, setSpecies] = useState(null);

  useEffect(() => {
    async function fetchTile() {
      let response = await axiosInstance.get(`/api/species/${id}`);
      setSpecies(response.data);
    }
    fetchTile();
  }, [id]);


  if (species === null) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    )
  }

  return (
    <Box align="center">
      <Paper className={classes.root}>
        <img src={species.image_url} alt={species.scientific_name} width="400" />
        <Box display='flex' flexDirection='column' align="center" width="80%">
          <Box display='flex' flexDirection='row' alignContent='center' textAlign="center">
            <TableContainer>
              <Table>
                <TableHead></TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>Species Name</TableCell>
                    <TableCell align="right">{species.scientific_name}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Common Name</TableCell>
                    <TableCell align="right">{species.main_common_name}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Order</TableCell>
                    <TableCell align="right">{species.order_name}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Family</TableCell>
                    <TableCell align="right">{species.family_name}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Environments</TableCell>
                    <TableCell align="right">
                      {species.environments.map((environment) => (
                        <div key={environment.code}>
                          <Link to={`/environments/${environment.code}`}>
                            {environment.habitat.toString()}
                          </Link>
                        </div>
                      ))}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Threats</TableCell>
                    <TableCell align="right">
                      {species.threats.map((threat) => (
                        <div key={threat.threat_id}>
                          <Link to={`/threats/${threat.threat_id}`}>
                            {threat.title.toString()}
                          </Link>
                        </div>
                      ))}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Countries</TableCell>
                    <TableCell align="right">
                      {
                        species.countries.map((country) => (
                          <div key={country.country_id}>
                            <p >{country.country}</p>
                          </div>
                        ))
                      }
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>

          {
            species.countries.length >= 1 &&
            (
              <Box>
                <GoogleMap location={species.countries[0].country} />
              </Box>
            )
          }
        </Box>
      </Paper>
    </Box>

  );
}

export default Species;