import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Paper, Toolbar, TextField, InputAdornment, FormControl, NativeSelect, TablePagination, CircularProgress, Checkbox } from '@material-ui/core/';
import Autocomplete from '@material-ui/lab/Autocomplete';
import SearchIcon from '@material-ui/icons/Search';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
  paper: {
    backgroundColor: 'LightBlue'
  },
  toolbar: {
    width: '95vw',
    margin: '15px',
    display: 'flex',
    flexDirection: "column",
    alignContent: "center"
  },
  select: {
    marginLeft: '0.5em',
    marginRight: '1em',
  },
  input: {
    backgroundColor: 'Azure',
    borderRadius: '5px',
    margin: '10px 10px',
  },
  inputRoot: {
    display: 'inline-block',
  },
  search: {
    display: 'inline-block',
    justifyContent: 'center',
    alignItems: 'center'
  },
  gridList: {
    width: '95vw',
    display: 'flex'
  },
  centeredToolbar: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  field: {
    display: "flex",
    alignContent: "center",
    alignItems: "center"
  }
}));

export default function TitlebarGridList(props) {
  let speciesData = props.species
  let environments = props.environments
  let threats = props.threats
  let pageNum = 100

  const classes = useStyles();

  // const [threats, setThreats] = useState(null);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(12);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('title');
  const [order, setOrder] = useState(0);
  const [filterSpecies, setFilterSpecies] = useState([]);
  const [filterEnvs, setFilterEnvs] = useState([]);

  // useEffect(() => {
  //   async function fetchThreats() {
  //     let response = await axiosInstance.get("/api/threats");
  //     setThreats(response.data);
  //   }
  //   fetchThreats();
  // }, []);

  const Sortables = [
    { id: 'title', label: 'Title' },
    { id: 'threat_id', label: 'Threat ID' },
    { id: 'code', label: 'Code' }
  ]

  // FILTER
  const handleChangeSort = event => {
    setSort(event.target.value)
    setPage(0);
  }

  const handleChangeFilterSpecies = event => {
    setFilterSpecies(event);
    setPage(0);
  }

  const handleChangeFilterEnvs = event => {
    setFilterEnvs(event);
    setPage(0);
  }

  //SEARCH
  const handleSearch = event => {
    setSearch(event.target.value);
    setPage(0);
  }

  //PAGINATION
  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  //SORTING
  const handleChangeOrder = event => {
    setOrder(parseInt(event.target.value, 10))
  }

  const Pagination = () => (<TablePagination
    page={page}
    rowsPerPageOptions={[6, 12, 24, 48]}
    rowsPerPage={rowsPerPage}
    labelRowsPerPage="Threats per Page: "
    count={pageNum}
    onChangePage={handleChangePage}
    onChangeRowsPerPage={handleChangeRowsPerPage}
  />)

  function Comparator(a, b) {
    if (a[sort] != null && b[sort] != null) {
      //handle codes differently
      if (sort === 'code'){
        var nums_a = (a[sort].split('.'))
        var nums_b = (b[sort].split('.'))
        for(var i = 0; i < nums_a.length; i++){
          if(nums_a[i] == null){ return order === 0 ? 1 : -1; }
          else if(nums_b[i] == null){ return order === 0 ? -1 : 1;}
          else if (nums_a[i] === nums_b[i]) {continue}
          else if (order === 0) {
            return parseInt(nums_a[i]) > parseInt(nums_b[i]) ? 1 : -1;
          } else {
            return parseInt(nums_a[i]) < parseInt(nums_b[i]) ? 1 : -1;
          }
        }
      }
      if (order === 0) {
        return a[sort] > b[sort] ? 1 : -1;
      } else {
        return a[sort] < b[sort] ? 1 : -1;
      }
    } else {
      return 0;
    }
  }

  function FilterSpecies(x) {
    for (let item in x.species){
      for (let searchItem in filterSpecies){
        if (x.species[item].scientific_name.toLowerCase().valueOf() === filterSpecies[searchItem].scientific_name.toLowerCase().valueOf()) {
          return true;
        }
      }
    }
    return false;
  }

  function FilterEnvs(x) {
    for (let item in x.environments){
      for (let searchItem in filterEnvs){
        if (x.environments[item].habitat.toLowerCase().valueOf() === filterEnvs[searchItem].habitat.toLowerCase().valueOf()) {
          return true;
        }
      }
    }
    return false;
  }

  function FilterBySearch(x) {
    for (let key in Sortables) {
      if (x[Sortables[key].id] != null && String(x[Sortables[key].id]).toLowerCase().includes(search)) {
        return true
      }
    }
    for (let item in x.species) {
      if (x.species[item].scientific_name.toLowerCase().includes(search)) {
        return true
      }
    }
    for (let item in x.environments) {
      if (x.environments[item].habitat.toLowerCase().includes(search)) {
        return true
      }
    }
    return false
  }

  const PagingAndSorting = () => {
    let filteredThreats = threats;
    if (filterSpecies.length > 0) {
      filteredThreats = filteredThreats.filter(x => FilterSpecies(x));
    }
    if (filterEnvs.length > 0) {
      filteredThreats = filteredThreats.filter(x => FilterEnvs(x));
    }
    if (search !== '') {
      filteredThreats = threats.filter(x => FilterBySearch(x))
    }
    pageNum = filteredThreats.length;
    return filteredThreats.sort(Comparator).slice(page * rowsPerPage, (page + 1) * rowsPerPage)
  }

  function getHighlightedText(text) {
    if (typeof (text) != "string") {
      text = String(text)
    }
    if (text != null) {
      // Split on highlight term and include term into parts, ignore case
      const parts = text.split(new RegExp(`(${search})`, 'gi'));
      return <span> {parts.map((part, i) =>
        <span key={i} style={part.toLowerCase() === search.toLowerCase() ? { fontWeight: 'bold', backgroundColor: 'yellow' } : {}}>
          {part}
        </span>)
      } </span>;
    }
  }

  if (speciesData === null || environments === null || threats === null) {
    return <div className={classes.root}><CircularProgress /></div>
  }

  return (
    <div className={classes.root}>
      <div className={classes.toolbar}>
        <Paper className={classes.paper}>
          <Toolbar className={classes.centeredToolbar}>
            <div className={classes.field}>
              <div>
                <p>Sort By: </p>
              </div>
              <div className={classes.select}>
                <FormControl fullWidth>
                  <NativeSelect variant='filled' value={sort} onChange={handleChangeSort}>
                    {
                      Sortables.map(Sortable => (
                        <option value={Sortable.id}>{Sortable.label}</option>))
                    }
                  </NativeSelect>
                </FormControl>
              </div>
            </div>
            <div className={classes.field}>
              <p>Order: </p>
              <div className={classes.select}>
                <FormControl fullWidth>
                  <NativeSelect variant='filled' value={order} onChange={handleChangeOrder}>
                    <option value={0}>Ascending</option>
                    <option value={1}>Descending</option>
                  </NativeSelect>
                </FormControl>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <p>Filters: </p>
              </div>
              <div>
                <Autocomplete
                  multiple
                  id="Species Filter"
                  options={speciesData}
                  disableCloseOnSelect 
                  limitTags={1}
                  size="small"
                  onChange = {(event, value) => handleChangeFilterSpecies(value)}
                  getOptionLabel={(option) => option.scientific_name}
                  defaultValue={[]}
                  value={filterSpecies}
                  renderOption={(option, { selected }) => (
                    <React.Fragment>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 2 }}
                        checked={selected}
                      />
                      {option.scientific_name}
                    </React.Fragment>
                  )}
                  style={{ width: 230 }}
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="Species" placeholder="Select" />
                  )}
                />
              </div>
              <div>
                <Autocomplete
                  multiple
                  id="Environments Filter"
                  options={environments}
                  disableCloseOnSelect 
                  limitTags={1}
                  size="small"
                  onChange = {(event, value) => handleChangeFilterEnvs(value)}
                  getOptionLabel={(option) => option.habitat}
                  defaultValue={[]}
                  value={filterEnvs}
                  renderOption={(option, { selected }) => (
                    <React.Fragment>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 2 }}
                        checked={selected}
                      />
                      {option.habitat}
                    </React.Fragment>
                  )}
                  style={{ width: 230 }}
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="Environments" placeholder="Select" />
                  )}
                />
              </div>
            </div>
            <div className={classes.input}>
              <TextField
                label='Search...'
                variant='outlined'
                size='small'
                onChange={handleSearch}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  )
                }}
              />
            </div>
          </Toolbar>
        </Paper>
      </div>
      <Pagination />
      <Grid container spacing={4}>
        {PagingAndSorting().map((tile) => (
          <Grid item xs={12} sm={6} md={4}>
            <Card>
              {/* <CardMedia
                component="img"
                alt={tile.title}
                height="300"
                image={tile.image_url}
              /> */}
              <CardContent>
                <Typography gutterBottom variant="h6" align='center'>
                  <Link to={`/threats/${tile.threat_id}`}>
                    {getHighlightedText(tile.title)}
                  </Link>
                </Typography>
                <Typography variant="body1" align='center'>
                  Code: {getHighlightedText(tile.code)}<br />
                  ID: {getHighlightedText(tile.threat_id)}<br />
                </Typography>
                {/* <Typography variant="h6" align='center'>
                  {getHighlightedText(tile.category)}
                </Typography> */}
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}