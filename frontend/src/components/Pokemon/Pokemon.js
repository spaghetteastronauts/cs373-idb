import { Card, CardContent, CardHeader, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { useState, useEffect } from 'react';
import MoveDisplay from './MoveDisplay';
import Moves from './Moves';
import { typeStylings } from './TypeStyling';

const useStyles = makeStyles((theme) => ({
  ...typeStylings,
  root: {
    width: "300px"
  },
  content: {
    display: "flex",
    flexDirection: "column"
  },
  movesRoot: {
    flexGrow: 1
  },
  spriteWrapper: {
    border: "1px solid black",
    borderRadius: "5px",
    padding: "0.25em",
    margin: "0.5em",
    width: "fit-content"
  },

}));

const Pokemon = ({ pokemon }) => {
  const classes = useStyles();

  let { type, name, move_list_id, sprite } = pokemon;
  

  // Controls which move's info to show in <MoveDisplay/>
  const [selectedMoveIdx, setSelectedMoveIdx] = useState(0);
  const [moves, setMoves] = useState(null);

  useEffect(() => {
    async function fetchMoves() {
      const baseUrl = "https://cs373-pokedex.com/api/get_specific_move/";

      let moveIds = getRandomSubarray(move_list_id, 4);
      let fetchedMoves = [];
      for (const id of moveIds) {
        let url = baseUrl + id;
        let response = await axios.get(url);

        // Formatting
        let move = response.data.response[0];
        move.name = splitAndCapitalize(move.name);
        move.accuracy = formatAccuracy(move.accuracy);
        move.power = formatPower(move.power);
        move.type = capitalizeFirstLetter(move.type);

        fetchedMoves.push(response.data.response[0]);
      }
      // console.log('fetchedMoves :>> ', fetchedMoves);
      setMoves(fetchedMoves);
    }
    fetchMoves();
  }, [move_list_id]);

  // Formatting
  let typeSpecificStyle = classes[type]
  type = type.toUpperCase();

  name = capitalizeFirstLetter(name);

  return (
    <Card className={classes.root}>
      {/* <Typography variant="h5">{name}</Typography> */}
      <CardHeader title={name} align="center" />
      <CardContent className={classes.content}>
        <Grid container justifyContent="center" spacing={2}>
          <Grid item>
            <Grid container justifyContent="center">
              <Grid item align="center">
                <div className={`${classes.type} ${typeSpecificStyle}`}>
                  {type}
                </div>
                <div className={classes.spriteWrapper}>
                  <img src={sprite} alt={`Sprite of ${sprite}`} />
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid item >
            <Moves
              moves={moves}
              selectedMoveIdx={selectedMoveIdx}
              setSelectedMoveIdx={setSelectedMoveIdx}
            />
          </Grid>
          <Grid item>
            <MoveDisplay moves={moves} selectedMoveIdx={selectedMoveIdx} />

          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function splitAndCapitalize(phrases) {
  let words = phrases.split('-');
  let capitalizedWords = words.map(word => capitalizeFirstLetter(word));
  return capitalizedWords.join(' ');
}

function formatPower(power) {
  if (power === 0) {
    return '---';
  }
  return power;
}

function formatAccuracy(accuracy) {
  if (accuracy === 0) {
    return '---';
  }
  return accuracy;
}

/**
 * https://stackoverflow.com/questions/11935175/sampling-a-random-subset-from-an-array
 * @param {*} arr 
 * @param {*} size 
 * @returns 
 */
function getRandomSubarray(arr, size) {
  let shuffled = arr.slice(0), i = arr.length, temp, index;
  while (i--) {
    index = Math.floor((i + 1) * Math.random());
    temp = shuffled[index];
    shuffled[index] = shuffled[i];
    shuffled[i] = temp;
  }
  return shuffled.slice(0, size);
}

export default Pokemon;



