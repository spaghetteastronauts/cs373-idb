import { Box, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

import { makeStyles } from '@material-ui/core/styles';

import { getSpecificTypeStyle, typeStylings } from './TypeStyling';

const useStyles = makeStyles((theme) => ({
  ...typeStylings,
  root: {
    // flexGrow: 1
    width: "250px"
  },
  moveEntry: {
    display: "flex",
    gap: "1ch",
    alignItems: "center"
  },
  moveName: {
    flexGrow: 1,
    textAlign: "left",
    height: "fit-content",
    fontSize: "1.1rem",
    whiteSpace: "nowrap",
    lineHeight: "1.2rem"
  },
}));



const Move = ({ move, idx, selectedMoveIdx, setSelectedMoveIdx }) => {
  const classes = useStyles();
  let isSelected = (idx === selectedMoveIdx);

  let specificTypeStyle = getSpecificTypeStyle(move.type, classes);
  let type = move.type.toUpperCase();

  function createHover(idx) {
    return () => {
      setSelectedMoveIdx(idx);
    }
  }

  return (
    <div key={move.id} className={classes.moveEntry} onMouseEnter={createHover(idx)}>
      <div className={`${classes.type} ${specificTypeStyle}`}>{type}</div>
      <div className={classes.moveName}>
        {move.name}{isSelected ? " ◂" : ''}
      </div>
    </div>
  );
}

const Moves = ({ moves, selectedMoveIdx, setSelectedMoveIdx }) => {
  const classes = useStyles();

  if (moves === null) {
    return (
      <Box align="left" className={classes.root}>
        <Typography variant="h6" component="h3">
          <Skeleton variant='text' width="6ch" />
        </Typography>
        <Skeleton variant="text" width="100%" height="2em"/>
        <Skeleton variant="text" width="100%" height="2em"/>
        <Skeleton variant="text" width="100%" height="2em"/>
        <Skeleton variant="text" width="100%" height="2em"/>
      </Box>)
  }

  return (
    <Box align="left" className={classes.root}>
      <Typography variant="h6" component="h3">Moves:</Typography>
      {moves.map((move, idx) => (
        <Move
          key={move.id}
          move={move}
          idx={idx}
          selectedMoveIdx={selectedMoveIdx}
          setSelectedMoveIdx={setSelectedMoveIdx}
        />
      ))}
    </Box>
  );
}

export default Moves;



