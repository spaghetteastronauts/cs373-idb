import { Box } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import { typeStylings } from './TypeStyling';

const useStyles = makeStyles((theme) => ({
  ...typeStylings,
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    gap: "1em",
    margin: "1em"
  },
  moveHeader: {
    display: "flex",
    justifyContent: "center",
    gap: "1em"
  },
  moveInfo: {
    display: "flex",
    flexDirection: "column",
    alignContent: "center",
    width: "fit-content"
  },
  infoEntry: {
    display: "flex",
    justifyContent: "space-between",
    gap: "1ch"
  }
}));

const MoveDisplay = ({ moves, selectedMoveIdx }) => {
  const classes = useStyles();

  // Waiting for API call
  if (moves === null) {
    return (
      <div>
        <Skeleton width="16ch" height="1.5em" />
        <Skeleton width="16ch" height="1.5em" />
        <Skeleton width="16ch" height="1.5em" />
      </div>
    )
  }

  let move = moves[selectedMoveIdx];
  let { accuracy, power, pp } = move;

  return (
    <Box align="center">
      <Box align="left" className={classes.moveInfo}>
        <div>
          <div className={classes.infoEntry}>
            <div>Accuracy:</div>
            <div>{accuracy}</div>
          </div>
          <div className={classes.infoEntry}>
            <div>Power:</div>
            <div>{power}</div>
          </div>
          <div className={classes.infoEntry}>
            <div>PP:</div>
            <div>{pp}</div>
          </div>
        </div>
      </Box>
    </Box>
  );
}




export default MoveDisplay;