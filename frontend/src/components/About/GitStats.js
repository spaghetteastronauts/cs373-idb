import { Box, Tooltip } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import TimelineIcon from '@material-ui/icons/Timeline';
import AdjustIcon from '@material-ui/icons/Adjust';
import BugReportIcon from '@material-ui/icons/BugReport';

const useStyles = makeStyles((theme) => ({
  statsBase: {
    display: 'flex',
    justifyContent: "space-around",
    margin: "auto",
  },
  statsItem: {
    display: 'flex',
    alignContent: 'center',
    verticalAlign: "center",
  },
  statsNumber: {
    marginTop: 2.5,
    marginLeft: "3px"
  }
}));

const GitStats = ({numCommits, numIssues, numUnitTests}) => {
  const classes = useStyles();

  return (
    <Box className={classes.statsBase}>
      <div className={classes.statsItem}>
        <Tooltip title="Number of Commits">
          <TimelineIcon />
        </Tooltip>
        <div className={classes.statsNumber}>{numCommits}</div>
      </div>
      <div className={classes.statsItem}>
        <Tooltip title="Number of Assigned Issues">
          <AdjustIcon />
        </Tooltip>
        <div className={classes.statsNumber}>{numIssues}</div>
      </div>
      <div className={classes.statsItem}>
        <Tooltip title="Number of Unit Tests">
          <BugReportIcon />
        </Tooltip>
        <div className={classes.statsNumber}>{numUnitTests}</div>
      </div>
    </Box>
  );
}

export default GitStats;