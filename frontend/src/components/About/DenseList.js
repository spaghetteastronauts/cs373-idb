import { List, ListItem, ListItemText } from "@material-ui/core";

const DenseList = ({ linesOfText }) => {
  const listItems = linesOfText.map(line => (
    <ListItem key={line}>
      <ListItemText
        primary={line}
      />
    </ListItem>
  ));

  return (
    <List dense={true}>
      {listItems}
    </List>
  );
}

export default DenseList;