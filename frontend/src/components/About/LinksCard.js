import { Typography, Card, CardContent, Container } from "@material-ui/core";

const LinksCard = () => {
  return (
    <Container maxWidth="md">
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h4" component="h2" align="center">
            Links
          </Typography>
          <Typography align="center">
            <a href="https://gitlab.com/evanhan65611/cs373-idb">GitLab Repo</a><br />
            <a href="https://gitlab.com/evanhan65611/cs373-idb/-/issues">GitLab Issue Tracker</a><br />
            <a href="https://gitlab.com/evanhan65611/cs373-idb/-/wikis/Title">GitLab Wiki</a><br />
            <a href="https://documenter.getpostman.com/view/16350932/TzedfPM5">API Documentation</a><br />
            <a href="https://apiv3.iucnredlist.org/">IUCN Red List API</a><br />
            <a href="https://documenter.getpostman.com/view/12094707/Tzm6kFR2#1f9ef64f-2a58-434b-9f60-43098714c72a">Postman Tests</a><br />
            <a href="https://speakerdeck.com/evanhan6561/cs373-indangered">Speaker Deck</a><br />

          </Typography>
        </CardContent>
      </Card>
    </Container>
  );
}

export default LinksCard;