import MemberCards from "./MemberCards";
import { Grid } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import RepoStatsCard from "./RepoStatsCard";
import ToolsCard from "./ToolsCard";
import LinksCard from "./LinksCard";
import UnitTestButton from "./UnitTestButton";

const useStyles = makeStyles((theme) => ({
  memberBase: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  base: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
}));

const AboutPage = () => {
  const classes = useStyles();

  return (
    <Grid className={classes.content} container spacing={3}>
      <Grid item xs={12}>
        <MemberCards />
      </Grid>
      <Grid item xs={12}>
        <RepoStatsCard />
      </Grid>
      <Grid item xs={12}>
        <LinksCard />
      </Grid>
      <Grid item xs={12}>
        <ToolsCard />
      </Grid>
      <Grid item xs={12}>
        <UnitTestButton />
      </Grid>
    </Grid>
  );
}

export default AboutPage;