import { Container, Card, CardContent, Typography, Grid } from "@material-ui/core";
import { useState, useEffect } from "react";
import { fetchRawCommits, fetchRawIssues } from "../../classes/GitLabAPI";
import axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import memberStaticData from '../../static/member-data.json';


const useStyles = makeStyles((theme) => ({
  statsItem: {
    display: 'flex',
    flexDirection: "column",
  },
}));

const RepoStats = ({ numCommits, numIssues, numUnitTests }) => {
  const classes = useStyles();

  return (
    <Grid container align="center" spacing={3}>
      <Grid item xs={12} sm={4}>
        <div className={classes.statsItem}>
          <Typography variant="h6" component="h3">Total Commits</Typography>
          <Typography variant="h6" component="h3">{numCommits}</Typography>
        </div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className={classes.statsItem}>
          <Typography variant="h6" component="h3">Total Issues</Typography>
          <Typography variant="h6" component="h3">{numIssues}</Typography>
        </div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className={classes.statsItem}>
          <Typography variant="h6" component="h3">Total Tests</Typography>
          <Typography variant="h6" component="h3">{numUnitTests}</Typography>
        </div>
      </Grid>
    </Grid>
  )
}

let numUnitTests = 0;

memberStaticData.members.forEach((member) => {
  numUnitTests += member.numUnitTests
})

const RepoStatsCard = () => {
  const classes = useStyles();

  const [totalCommits, setTotalCommits] = useState(null);
  const [totalIssues, setTotalIssues] = useState(null);

  

  useEffect(() => {
    const source = axios.CancelToken.source();

    async function fetchData() {
      const rawCommits = await fetchRawCommits(source);
      if (rawCommits !== null) { setTotalCommits(rawCommits.length); }

      const rawIssues = await fetchRawIssues(source);
      if (rawIssues !== null) { setTotalIssues(rawIssues.length); }
    }
    fetchData();

    return () => {
      source.cancel('CANCELLED');
    }
  }, []);

  return (
    <Container maxWidth="sm">
      <Card className={classes.base}>
        <CardContent>
          <Typography gutterBottom variant="h4" component="h2" align="center">
            Project Stats
          </Typography>
          <RepoStats
            numCommits={totalCommits}
            numIssues={totalIssues}
            numUnitTests={numUnitTests}
          />
        </CardContent>
      </Card>
    </Container>
  );
}

export default RepoStatsCard;