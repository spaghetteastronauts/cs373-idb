import React from 'react';
import { Link } from 'react-router-dom';
import { useParams } from "react-router";
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

// const useStyles = makeStyles((theme) => ({

//   }));

export default function SearchResults(props) {

    let species = props.species;
    let environments = props.environments;
    let threats = props.threats;

    let { search } = useParams();


    const SortablesSpecies = [
        { id: 'main_common_name', label: 'Common Name' },
        { id: 'category', label: 'Category' },
        { id: 'family_name', label: 'Family Name' },
        { id: 'order_name', label: 'Order Name' },
        { id: 'scientific_name', label: 'Scientific Name' },
    ]

    const SortablesEnvironments = [
        { id: 'habitat', label: 'Habitat' },
        { id: 'code', label: 'Code' },
    ]

    const SortablesThreats = [
        { id: 'title', label: 'Title' },
        { id: 'threat_id', label: 'Threat ID' },
        { id: 'code', label: 'Code' }
    ]


    function FilterBySearchSpecies(x) {
        for (let key in SortablesSpecies) {
            if (x[SortablesSpecies[key].id] != null && String(x[SortablesSpecies[key].id]).toLowerCase().includes(search)) {
                return true
            }
        }
        for (let item in x.environments) {
            if (x.environments[item].habitat.toLowerCase().includes(search)) {
                return true
            }
        }
        for (let env in x.threats) {
            if (x.threats[env].title.toLowerCase().includes(search)) {
                return true
            }
        }
        return false
    }

    function FilterBySearchEnvironments(x) {
        for (let key in SortablesEnvironments) {
            if (x[SortablesEnvironments[key].id] != null && String(x[SortablesEnvironments[key].id]).toLowerCase().includes(search)) {
                return true
            }
        }
        for (let item in x.species) {
            if (x.species[item].scientific_name.toLowerCase().includes(search)) {
                return true
            }
        }
        for (let item in x.threats) {
            if (x.threats[item].title.toLowerCase().includes(search)) {
                return true
            }
        }
        return false
    }

    function FilterBySearchThreats(x) {
        for (let key in SortablesThreats) {
            if (x[SortablesThreats[key].id] != null && String(x[SortablesThreats[key].id]).toLowerCase().includes(search)) {
                return true
            }
        }
        for (let item in x.species) {
            if (x.species[item].scientific_name.toLowerCase().includes(search)) {
                return true
            }
        }
        for (let item in x.environments) {
            if (x.environments[item].habitat.toLowerCase().includes(search)) {
                return true
            }
        }
        return false
    }


    function getHighlightedText(text) {
        if (typeof (text) != "string") {
            text = String(text)
        }
        if (text != null) {
            // Split on highlight term and include term into parts, ignore case
            const parts = text.split(new RegExp(`(${search})`, 'gi'));
            return <span> {parts.map((part, i) =>
                <span key={i} style={part.toLowerCase() === search.toLowerCase() ? { fontWeight: 'bold', backgroundColor: 'yellow' } : {}}>
                    {part}
                </span>)
            } </span>;
        }
    }

    if (species === null || environments === null || threats === null) {
        return (<h1>Loading...</h1>)
    }

    return (
        <div>
            <h1>
                Search results for {search}
            </h1>
            <hr />
            <h2>
                Species
            </h2>
            <Grid container spacing={4}>
                {species.filter(x => FilterBySearchSpecies(x)).map((tile) => (
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardContent>
                                <Typography gutterBottom variant="h6" align='center'>
                                    {getHighlightedText(tile.main_common_name)}
                                </Typography>
                                <Typography variant="body1" align='center'>
                                    {getHighlightedText(tile.order_name)} {getHighlightedText(tile.family_name)} <br />
                                    <Link to={`/species/${tile.scientific_name}`}>
                                        {getHighlightedText(tile.scientific_name)}
                                    </Link>
                                </Typography>
                                <Typography variant="h6" align='center'>
                                    {getHighlightedText(tile.category)}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <h2>
                Environments
            </h2>
            <Grid container spacing={4}>
                {environments.filter(x => FilterBySearchEnvironments(x)).map((tile) => (
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardContent>
                                <Typography gutterBottom variant="h6" align='center'>
                                    <Link to={`/environments/${tile.code}`}>
                                        {getHighlightedText(tile.habitat)}
                                    </Link>
                                </Typography>
                                <Typography variant="body1" align='center'>
                                    {getHighlightedText(tile.suitability)}
                                </Typography>
                                <Typography variant="h6" align='center'>
                                    Code: {getHighlightedText(tile.code)}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <h2>
                Threats
            </h2>
            <Grid container spacing={4}>
                {threats.filter(x => FilterBySearchThreats(x)).map((tile) => (
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>                         
                            <CardContent>
                                <Typography gutterBottom variant="h6" align='center'>
                                    <Link to={`/threats/${tile.threat_id}`}>
                                        {getHighlightedText(tile.title)}
                                    </Link>
                                </Typography>
                                <Typography variant="body1" align='center'>
                                    Code: {getHighlightedText(tile.code)}<br />
                                    ID: {getHighlightedText(tile.threat_id)}<br />
                                </Typography>
                                {/* <Typography variant="h6" align='center'>
                                    {getHighlightedText(tile.category)}
                                </Typography> */}
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>

        </div>
    )
}
