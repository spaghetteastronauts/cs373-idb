import axios from "axios";

const axiosInstance = axios.create(
    {
        baseURL: "https://indangered.me/"
    }
)

export default axiosInstance;