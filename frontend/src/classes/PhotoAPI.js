function SearchPhotos(name, elementID) {
  let clientID = "lLFOjnCUFLrqxCibg_llj5QcBO9rk7RlXVGq9YbXZOQ";
  let token = name.split(" ");
  let query = token[token.length - 1];
  let url = "https://api.unsplash.com/search/photos/?client_id="+clientID+"&query="+query;
  let result = "";
  fetch(url).then(response => {
    return response.json();
  }).then(json => {
    if (json.results.length > 0)
      return json.results[0].urls.thumb;
    else
      throw 'error!';
  }).then(url => {
    result = url;
    document.getElementById(elementID).src = url;
  }).catch(error => {
    console.log(error);
  });
  return result;
}

export { SearchPhotos }