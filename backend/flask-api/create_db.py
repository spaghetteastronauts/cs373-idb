import json
from models import Species, Environment, Threat, Country
from setup import db
from JsonPopulator import JsonPopulator

def load_json(filename):
    with open(filename, encoding='UTF-8') as file:
        jsn = json.load(file)
        file.close()
    return jsn

def main():
    species_json = load_json("json/species.json")
    environ_json = load_json("json/environment.json")
    threat_json = load_json("json/threat.json")
    # species_json = load_json("json/species_with_url.json")
    # environ_json = load_json("json/environment_with_url.json")
    # threat_json = load_json("json/threat_with_url.json")
    country_json = load_json("json/country.json")
    

    species_columns = [
    "scientific_name", 
    "main_common_name",
    "family_name", 
    "order_name", 
    "category",
    "image_url"
    ]
    environ_columns = ["code", "habitat", "image_url"]
    threat_columns = ["title", "code", "invasive", "image_url"]
    country_columns = ["code", "country"]

    # Populate the Tables
    SpeciesPopulator = JsonPopulator(species_columns, Species, species_json, db)
    EnvironPopulator = JsonPopulator(environ_columns, Environment, environ_json, db)
    ThreatPopulator = JsonPopulator(threat_columns, Threat, threat_json, db)
    CountryPopulator = JsonPopulator(country_columns, Country, country_json, db)

    # And then link the ORM objects together
    # Link Species to Environment
    for raw_species in species_json:
        species_orm = SpeciesPopulator.get_orm(raw_species)
        raw_environs = raw_species['environments']
        environ_orm_list = [EnvironPopulator.get_orm(raw_environ) for raw_environ in raw_environs]

        species_orm.environments += environ_orm_list

    # Link Species to Threat
    for raw_species in species_json:
        species_orm = SpeciesPopulator.get_orm(raw_species)
        raw_countries = raw_species['threats']
        country_orm_list = [ThreatPopulator.get_orm(raw_threat) for raw_threat in raw_countries]

        species_orm.threats += country_orm_list

    # Link Species to Country
    for raw_species in species_json:
        species_orm = SpeciesPopulator.get_orm(raw_species)
        raw_countries = raw_species['countries']
        country_orm_list = [CountryPopulator.get_orm(raw_country) for raw_country in raw_countries]

        species_orm.countries += country_orm_list
        
    # Link Environment to Threat
    for raw_environ in environ_json:
        environ_orm = EnvironPopulator.get_orm(raw_environ)
        raw_threats = raw_environ['threats']
        threat_orm_list = [ThreatPopulator.get_orm(raw_threat) for raw_threat in raw_threats]
        
        environ_orm.threats += threat_orm_list

    db.session.commit()

# Drop everything and recreate tables
db.drop_all()
db.create_all()

main()