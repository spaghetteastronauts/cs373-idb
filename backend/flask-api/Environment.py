from models import Environment
from sqlalchemy import or_, desc



def sort_environment(query, args):
    environments_orderable = ["habitat","threat","species"]
    if "order_by" in args:
        order_by = args["order_by"]
    else:
        order_by = "habitat"
    if "ordering" in args:
        ordering = args["ordering"]
    else:
        ordering = "desc"
    if not order_by or order_by not in environments_orderable:
        order_by = "habitat"
    if ordering == "desc":
        query.order_by(
            desc(getattr(Environment, order_by)))
    else:
        query.order_by(
            getattr(Environment, order_by))
    return query


def filter_environment(query, args):
    if "habitat" in args:
        habitat = args["habitat"]
        query = query.filter(or_(Environment.habitat == habitat))
   
    if "threat_id" in args:
        threat_id = args["threat_id"]
        query = query.join(Environment.threats).filter_by(threat_id=threat_id)

    if "species_scientific_name" in args:
        species_scientific_name = args["species_scientific_name"]
        query = query.join(Environment.houses).filter_by(scientific_name=species_scientific_name)

    return query


