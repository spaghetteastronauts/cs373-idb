import unittest
import os
import sys
from models import db, Species, Environment, Threat


class TestDBCases(unittest.TestCase):
    
    #inserting a specie
    def test_source_insert_specie_1(self):
        Threat1 = Threat(threat_id = 1000, title = "title 1", code = "code 1", invasive = "inv 1")
        Environment1 = Environment(habitat = "habitat 1", code = "1.111", threats = [Threat1])
        s = Species(scientific_name = 'scientific name 1',
        main_common_name = 'main common name 1',
        family_name = 'family name 1',
        order_name = 'order name 1',
        category = 'category 1',
        environments = [Environment1],
        threats = [Threat1]
                    )
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Species).filter_by(scientific_name = 'scientific name 1').one()
        e = db.session.query(Environment).filter_by(code = '1.111').one()
        self.assertEqual(str(r.scientific_name),'scientific name 1')
        r.environments = []
        r.threats = []
        e.threats = []
        db.session.commit()
        db.session.query(Species).filter_by(scientific_name = 'scientific name 1').delete()
        db.session.query(Environment).filter_by(code = '1.111').delete()
        db.session.query(Threat).filter_by(threat_id = 1000).delete()
        db.session.commit()

    #inserting a specie
    def test_source_insert_specie_2(self):
        Threat1 = Threat(threat_id = 1000, title = "title 1", code = "code 1", invasive = "inv 1")
        Environment1 = Environment(habitat = "habitat 1", code = "1.111", threats = [Threat1])
        s = Species(scientific_name = 'scientific name 2',
        main_common_name = 'main common name 2',
        family_name = 'family name 2',
        order_name = 'order name 2',
        category = 'category 2',
        environments = [Environment1],
        threats = [Threat1]
                    )
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Species).filter_by(scientific_name = 'scientific name 2').one()
        e = db.session.query(Environment).filter_by(code = '1.111').one()
        self.assertEqual(str(r.scientific_name),'scientific name 2')
        r.environments = []
        r.threats = []
        e.threats = []
        db.session.commit()
        db.session.query(Species).filter_by(scientific_name = 'scientific name 2').delete()
        db.session.query(Environment).filter_by(code = '1.111').delete()
        db.session.query(Threat).filter_by(threat_id = 1000).delete()
        db.session.commit()
    
    #inserting a specie
    def test_source_insert_specie_3(self):
        Threat1 = Threat(threat_id = 3000, title = "title 3", code = "code 3", invasive = "inv 3")
        Environment1 = Environment(habitat = "habitat 3", code = "3.333", threats = [Threat1])
        s = Species(scientific_name = 'scientific name 3',
        main_common_name = 'main common name 3',
        family_name = 'family name 3',
        order_name = 'order name 3',
        category = 'category 3',
        environments = [Environment1],
        threats = [Threat1]
                    )
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Species).filter_by(scientific_name = 'scientific name 3').one()
        e = db.session.query(Environment).filter_by(code = '3.333').one()
        self.assertEqual(str(r.scientific_name),'scientific name 3')
        r.environments = []
        r.threats = []
        e.threats = []
        db.session.commit()
        db.session.query(Species).filter_by(scientific_name = 'scientific name 3').delete()
        db.session.query(Environment).filter_by(code = '3.333').delete()
        db.session.query(Threat).filter_by(threat_id = 3000).delete()
        db.session.commit()
    
    #inserting an environment
    def test_source_insert_environment_1(self):
        Threat1 = Threat(threat_id = 1000, title = "title 1", code = "code 1", invasive = "inv 1")
        s = Environment(habitat = "habitat 1", code = "1.111", threats = [Threat1])
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Environment).filter_by(code = '1.111').one()
        self.assertEqual(str(r.code),'1.111')
        r.threats = []
        db.session.commit()
        db.session.query(Environment).filter_by(code = '1.111').delete()
        db.session.query(Threat).filter_by(threat_id = 1000).delete()
        db.session.commit()


    #inserting an environment
    def test_source_insert_environment_2(self):
        Threat1 = Threat(threat_id = 1000, title = "title 1", code = "code 1", invasive = "inv 1")
        s = Environment(habitat = "habitat 2", code = "2.222", threats = [Threat1])
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Environment).filter_by(code = '2.222').one()
        self.assertEqual(str(r.code),'2.222')
        r.threats = []
        db.session.commit()
        db.session.query(Environment).filter_by(code = '2.222').delete()
        db.session.query(Threat).filter_by(threat_id = 1000).delete()
        db.session.commit()

    #inserting an environment
    def test_source_insert_environment_3(self):
        Threat1 = Threat(threat_id = 3000, title = "title 3", code = "code 3", invasive = "inv 3")
        s = Environment(habitat = "habitat 3", code = "3.333", threats = [Threat1])
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Environment).filter_by(code = '3.333').one()
        self.assertEqual(str(r.code),'3.333')
        r.threats = []
        db.session.commit()
        db.session.query(Environment).filter_by(code = '3.333').delete()
        db.session.query(Threat).filter_by(threat_id = 3000).delete()
        db.session.commit()

    #inserting an threat
    def test_source_insert_threat_1(self):
        s = Threat(threat_id = 1000, title = "title 1", code = "code 1", invasive = "inv 1")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Threat).filter_by(threat_id = 1000).one()
        self.assertEqual(r.threat_id, 1000) 
        r.threats = []
        db.session.commit()
        db.session.query(Threat).filter_by(threat_id = 1000).delete()
        db.session.commit()



    #inserting a threat
    def test_source_insert_threat_2(self):
        s = Threat(threat_id = 2000, title = "title 2", code = "code 2", invasive = "inv 2")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Threat).filter_by(threat_id = 2000).one()
        self.assertEqual(r.threat_id, 2000)
        r.threats = []
        db.session.commit()
        db.session.query(Threat).filter_by(threat_id = 2000).delete()
        db.session.commit()

    #inserting a threat
    def test_source_insert_threat_3(self):
        s = Threat(threat_id = 3000, title = "title 3", code = "code 3", invasive = "inv 3")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Threat).filter_by(threat_id = 3000).one()
        self.assertEqual(r.threat_id, 3000)
        r.threats = []
        db.session.commit()
        db.session.query(Threat).filter_by(threat_id = 3000).delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()