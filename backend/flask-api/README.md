# Pre-requisites
* **Python >3.5** and **pip** must be installed
* The package **virtualenv** must be installed

# Flask Environment Setup
Make sure you're in the backend/flask-api directory. The bash install
script will setup the environment for you, but you still need to activate it.

## Installing the Virtual Environment
```
./install
```

## Activating the Virtual Environment
Unfortunately, the command to the virtual environment is OS dependent.

On Windows:
```
source venv/Scripts/activate
```

On Mac/Linux:
```
source venv/bin/activate
```

## Running the Development Flask API
*Python may be referenced differently on Mac/Linux*
Make sure that at the bottom of backend/flask-api/main.py,
`app.run(debug=True)` to enable flask's development mode.
While in the backend/flask-api directory, enter:
```
python main.py
```

## Running the Production Flask API
*Python may be referenced differently on Mac/Linux*
Make sure that at the bottom of backend/flask-api/main.py,
`app.run(debug=False)` to disable flask's development mode.
While in the backend/flask-api directory, enter:
```
python main.py
```