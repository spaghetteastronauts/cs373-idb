from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import os

# Load confidential data as environment variables from .env file
load_dotenv()
app = Flask(__name__, static_folder="build")


# Configuration
DB_USER = os.environ.get("DB_USER", "postgres")
DB_PASS = os.environ.get("DB_PASS", "PASSWORD")
DB_HOST = os.environ.get("DB_HOST", "localhost:5432")
DB_NAME = os.environ.get("DB_NAME", "pillarsdb")

if os.environ.get("ENVIRONMENT") == "Local":
    DB_USER = "postgres"
    DB_HOST = "localhost:5432"
    DB_NAME = "pillarsdb"


# Gitlab CI specific conditions
if os.environ.get("GITLAB_CI") is not None:
    DB_HOST = "postgres:5432"
    DB_PASS = "password"


# Cloud SQL Proxy - 172.17.0.1:5432
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql+psycopg2://{DB_USER}:{DB_PASS}@{DB_HOST}/{DB_NAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # To suppress a warning message


db_config = {
        "pool_size": 5,
        "max_overflow": 2,
        "pool_timeout": 10,
        "pool_recycle": 1800,  # 30 minutes
}
db = SQLAlchemy(app, engine_options=db_config)
