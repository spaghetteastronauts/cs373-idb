from collections import namedtuple


class JsonPopulator:
    """
    Takes in JSON list and uses it to populate a database table.
        column_names: [String] - 
        The list of attributes to extract from the JSON

        model: SQLAlchemy.Model -
        The SQLAlchemy ORM class that the JSON will be converted to

        raw_entries: [dict]
        The list of JSON objects to parse

        db: SQLAlchemy
        The database

    """
    def __init__(self, column_names, model, raw_entries, db):
        self.column_names = column_names
        self.model = model

        model_name = model.__name__
        self.ModelTuple = namedtuple(f'{model_name}Tuple', column_names)

        self._model_tup_to_orm = self.populate_table(raw_entries, db)

    def create_model_tuple(self, raw_entry):
        model_fields = JsonPopulator.filter_dict_keys(
            self.column_names, raw_entry)
        model_tuple = self.ModelTuple(**model_fields)
        return model_tuple

    def create_orm(self, model_tuple):
        orm = self.model(**(model_tuple._asdict()))
        return orm

    def get_orm(self, raw_entry):
        model_tuple = self.create_model_tuple(raw_entry)
        return self._model_tup_to_orm[model_tuple]

    def populate_table(self, raw_entries, db):
        """
        Populates a database table with rows parsed from a JSON list.

        raw_entries: [dict]
        The list of JSON objects to parse

        db: SQLAlchemy
        The database

        @return
        tuple_to_orm: <ModelTuple: Model>
        A dictionary that maps tuples to their corresponding ORM versions.
        It can be used to quickly find a tuple's corresponding ORM version
        which aids in creating associations.

        """
        tuple_to_orm = {}
        for raw_entry in raw_entries:
            model_tuple = self.create_model_tuple(raw_entry)

            if model_tuple not in tuple_to_orm:
                tuple_to_orm[model_tuple] = self.create_orm(model_tuple)

        db.session.add_all(tuple_to_orm.values())
        db.session.commit()
        return tuple_to_orm

    @staticmethod
    def filter_dict_keys(white_list, dictionary):
        """
        Grabs only the key-value pairs with keys in 'white_list' from 'dictionary'.
        Throws an exception if 'dictionary' doesn't have a key in 'white_list'
        """
        result = {}
        for key in white_list:
            result[key] = dictionary[key]
        return result
