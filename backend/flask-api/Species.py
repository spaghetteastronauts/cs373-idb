from models import Species, Threat
from sqlalchemy import or_, desc

def sort_species(query, args):
    species_orderable = ["category","threat","environment","scientific_name","order_name","common_name"]
    if "order_by" in args:
        order_by = args["order_by"]
    else:
        order_by = "scientific_name"
    if "ordering" in args:
        ordering = args["ordering"]
    else:
        ordering = "desc"
    if not order_by or order_by not in species_orderable:
        order_by = "common_name"
    if ordering == "desc":
        query.order_by(
            desc(getattr(Species, order_by)))
    else:
        query.order_by(
            getattr(Species, order_by))
    return query

def filter_species(query, args):
    if "order_name" in args:
        order_name = args["order_name"]
        query = query.filter(or_(Species.order_name == order_name))
    
    if "family_name" in args:
        family_name = args["family_name"]
        query = query.filter(or_(Species.family_name == family_name))

    if "scientific_name" in args:
        scientific_name = args["scientific_name"]
        query = query.filter(or_(Species.scientific_name == scientific_name))

    if "category" in args:
        category = args["category"]
        query = query.filter(or_(Species.category == category))

    if "threat_id" in args:
        threat_id = args["threat_id"]
        query = query.join(Species.threats).filter_by(threat_id=threat_id)

    if "environment_code" in args:
        environment_code = args["environment_code"]
        query = query.join(Species.environments).filter_by(code=environment_code)

    return query
